FROM almalinux:8
COPY mock/dovecot.rpm dovecot.rpm
RUN dnf install -y dovecot.rpm
EXPOSE 24 110 143 587 993 995
CMD ["/usr/sbin/dovecot","-F"]
